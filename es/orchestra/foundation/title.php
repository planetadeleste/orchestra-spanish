<?php

    return array(
      'home'            => 'Inicio',
      'login'           => 'Iniciar sesión',
      'logout'          => 'Salir',
      'register'        => 'Registrarse',
      'remember-me'     => 'Recordarme',
      'forgot-password' => 'Olvide mi clave',
      'reset-password'  => 'Resetear clave',
      'back-to-login'   => 'Volver a inicio de sesión',
      'account'         => array(
        'profile'  => 'Editar Perfil',
        'password' => 'Editar Clave',
      ),
      'extensions'      => array(
        'list'      => 'Extensiones',
        'configure' => 'Configurar Extensión',
      ),
      'resources'       => array(
        'list'        => 'Recursos',
        'list-detail' => 'Lista de recursos disponibles',
      ),
      'settings'        => array(
        'list' => 'Configuraciones',
      ),
      'publisher'       => array(
        'ftp'         => 'Credenciales FTP',
        'description' => 'Es necesario disponer de sus credenciales para realizar la siguiente tarea'
      ),
      'users'           => array(
        'list'   => 'Usuarios',
        'create' => 'Nuevo Usuario',
        'update' => 'Editar Usuario',
      ),
    );
