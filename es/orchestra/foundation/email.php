<?php

return array(
    'credential' => array(
        'register' => '[:site] Su nueva cuenta',
    ),
    'forgot'     => array(
        'request' => "[:site] Resetear su clave",
        'reset'   => "[:site] Su nueva clave",
    ),
);
