<?php

return array(
    'add'    => 'Add',
    'cancel' => 'Cancel',
    'delete' => 'Delete',
    'edit'   => 'Edit',
    'next'   => 'Next',
    'submit' => 'Submit',
    'view'   => 'View',

    'name'        => 'Nombre',
    'description' => 'Descripción',

    'no-data'      => 'Sin datos hasta el momento',
    'no-extension' => 'Sin extensiones disponibles',

    'account' => array(
        'current_password' => 'Current Password',
        'new_password'     => 'New Password',
        'confirm_password' => 'Confirm Password',
    ),

    'email' => array(
        'driver'          => 'Driver',
        'from'            => 'From Address',
        'host'            => 'Host',
        'port'            => 'Port',
        'username'        => 'Username',
        'password'        => 'Password',
        'encryption'      => 'Encryption',
        'change_password' => 'Change Password',
        'command'         => 'Sendmail Command',
        'queue'           => 'Mail via Queue',
    ),

    'extensions' => array(
        'name'         => 'Extension',
        'author'       => 'Por :author',
        'version'      => 'Versión :version',
        'dependencies' => 'Dependencias',
        'update'       => 'Actualización',
        'handles'      => 'Handle URL',

        'actions' => array(
            'activate'   => 'Activar',
            'deactivate' => 'Desactivar',
            'update'     => 'Migrar y Publicar',
        ),

        'publisher' => array(
            'host'            => 'Servidor',
            'user'            => 'Usuario',
            'password'        => 'Clave',
            'connection-type' => 'Tipo de conexión',
        ),
    ),

    'search'    => array(
        'button'  => 'Buscar',
        'filter'  => 'Filtro',
        'keyword' => 'Buscar clave...',
    ),

    'settings' => array(
        'application'       => 'Aplicación',
        'mail'              => 'E-mail',
        'user-registration' => 'Permitir a los usuarios registrarse',
    ),

    'users' => array(
        'email'    => 'E-mail',
        'username' => 'Usuario',
        'fullname' => 'Nombre completo',
        'password' => 'Clave',
        'roles'    => 'Roles',
    ),
);
