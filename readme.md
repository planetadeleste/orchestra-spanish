Orchestra Spanish
=================
---
Paquete de idioma para Orchestra Platform.

Instalación
--------------
Dentro del directorio `/app/lang/packages`:
```sh
git clone https://planetadeleste@bitbucket.org/planetadeleste/orchestra-spanish.git
```

Configuración
-------------
Editar el archivo `/app/config/app.php` con el locale "es".
```php
    return array(
    /*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'es',
    );
```


License
----

MIT